package de.fhbingen.epro.vl3.ioc.autowiring;

import org.springframework.stereotype.Component;

@Component
public class AlternateMessage {
	
	public String getAlternativeMessage() {
		return "This is an alternative message";
	}

}
