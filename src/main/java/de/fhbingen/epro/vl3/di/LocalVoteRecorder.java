package de.fhbingen.epro.vl3.di;

import java.util.HashMap;
import java.util.Map;

import de.fhbingen.epro.vl3.model.Candidate;

public class LocalVoteRecorder implements VoteRecorder {

    Map<Candidate, Integer> hVotes = new HashMap<Candidate, Integer>();

    public void record(Candidate candidate) {
        int count = 0;

        if (!hVotes.containsKey(candidate)){
            hVotes.put(candidate, count);
        } else {
            count = hVotes.get(candidate);
        }

        count++;

        hVotes.put(candidate, count);
    }

}
