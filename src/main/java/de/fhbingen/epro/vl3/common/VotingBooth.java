package de.fhbingen.epro.vl3.common;

import java.util.HashMap;
import java.util.Map;

import de.fhbingen.epro.vl3.model.Candidate;

public class VotingBooth {

    VoteRecorder voteRecorder = new VoteRecorder();

    public void vote(Candidate candidate) {
        voteRecorder.record(candidate);
    }

    class VoteRecorder {
        Map<Candidate, Integer> hVotes = new HashMap<Candidate, Integer>();

        public void record(Candidate candidate) {
            int count = 0;

            if (!hVotes.containsKey(candidate)){
                hVotes.put(candidate, count);
            } else {
                count = hVotes.get(candidate);
            }

            count++; 

            hVotes.put(candidate, count);  

        }
    }

}
